<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\cache\common
{
	interface CacheHandler
	{
		/**
		 * Stores data in to the cache for the $key. 
		 * @param string $cacheKey
		 * @param mixed  $data Any kind of data including arrays.
		 * @param int    $expiryTime Given in seconds.
		 * @param string $subFolder
		 * @return mixed
		 */
		public function store(string $cacheKey, mixed $data, int $expiryTime=0, ?string $subFolder=null):bool;
		
		/**
		 * Retrieves data from cache. It returns FALSE when no data is found. 
		 * @param string $cacheKey
		 * @param string $subFolder
		 * @return mixed
		 */
		public function retrieve(string $cacheKey, ?string $subFolder=null):mixed;
		
		/**
		 * Clears the cache. In the case of a file system cache, clears all cache files.
		 * @param string $subFolder
		 */
		public function clear(?string $subFolder=null):bool;
		
		/**
		 * Removes the key from the cache.
		 * @param string $cacheKey
		 * @param string $subFolder
		 */
		public function free(string $cacheKey, ?string $subFolder=null):bool;
	}
}
