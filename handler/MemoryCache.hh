<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\cache\handler
{
	use nuclio\core\plugin\Plugin;
	use nuclio\core\ClassManager;
	use nuclio\plugin\config\Config;
	use nuclio\plugin\cache\common\CacheHandler;
	use nuclio\plugin\cache\CacheException;
	use nuclio\plugin\config\ConfigCollection;

	use \Memcached;

	<<singleton>>
	class MemoryCache extends Plugin implements CacheHandler
	{
		private Memcached $cache;
		private string    $appName;

		public static function getInstance(/* HH_FIXME[4033] */...$args):MemoryCache
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(ConfigCollection $config):void
		{
			parent::__construct();
			if (!$config instanceof Map)
			{
				throw new CacheException('Controller must accept a config map.');
			}
			$appName = $config->get('prefix');
			if (is_string($appName))
			{
				$this->appName = $appName;
			}
			else
			{
				throw new CacheException("Application name not a string.");
			}
			$this->cache = new Memcached();
			$this->cache->addServer('localhost', 11211);
		}

		/**
		 * Stores data in to the cache for the $key. 
		 * @param string $cacheKey
		 * @param mixed  $data
		 * @param int    $expiryTime
		 * @param string $folder 		Not used. Present for consistency with CacheHandler interface
		 * @return bool 	true
		 */
		public function store(string $cacheKey, mixed $data, int $expiryTime=0, ?string $folder=null):bool
		{
			if($expiryTime)
			{
				$expiryTime = time()+$expiryTime;
			}
			$result = $this->cache->set($this->appName.$cacheKey, $data, $expiryTime);
			if($result)
			{
				return true;
			}
			else
			{
				throw new CacheException($this->getMessage());
			}
		}
		
		/**
		 * Retrieves data from cache. It returns FALSE when no data is found. 
		 * @param string $cacheKey
		 * @return mixed
		 */
		public function retrieve(string $cacheKey, ?string $subFolder=null):mixed
		{
			return $this->cache->get($this->appName.$cacheKey);
		}
		
		/**
		 * Clears the cache
		 * @param string $subFolder	Not used. Present for consistency with CacheHandler interface
		 * @return bool || string
		 */
		public function clear(?string $subFolder=null):bool
		{
			// return $this->cache->flush();
			$keys = $this->cache->getAllKeys();
			$return = true;
			foreach ($keys as $key)
			{
				$result =$this->cache->delete($key);
				if(!$result)
				{
					$retrun = false;
				}
			}
			return $return;
		}
		
		/**
		 * Removes the key from the cache.
		 * @param string $cacheKey
		 * @param string $subFolder	Not used. Present for consistency with CacheHandler interface
		 * @return bool || string
		 */
		public function free(string $cacheKey, ?string $subFolder=null):bool
		{
			return $this->cache->delete($this->appName.$cacheKey);
		}
		
		public function getCode():int
		{
			return $this->cache->getResultCode();
		}
		
		public function getMessage():string
		{
			return $this->cache->getResultMessage();
		}
	}
}
