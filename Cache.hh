<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\cache
{
	use nuclio\Nuclio;
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;

	use nuclio\plugin\cache\CacheException;
	use nuclio\plugin\cache\common\CacheHandler;
	use nuclio\plugin\cache\handler\MemoryCache;
	use nuclio\plugin\config\ConfigCollection;
	/**
	 * Cache plugin 
	 * This plugin is used to save and manipulate cached data.
	 * @package    nuclio\plugin\cache
	 */
	<<singleton>>
	class Cache extends Plugin
	{
		/**
		 * This property stores an object that manages the cache.
		 * 
		 * @property CacheHandler
		 * @access protected
		 */
		protected CacheHandler $handler;


		/**
		 * Get an instance by implementing the singleton desing pattern.
		 *
		 * @param      ...     $args   set of parameters to be sent to the constructor (ConfigCollection $config, string $type)
		 *
		 * @return     Cache  Instance of the current class.
		 * @access public static
		 */
		public static function getInstance(/* HH_FIXME[4033] */...$args):Cache
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		
		/**
		 * Setup the configuration related to the cache object and load an object based on the type parameter.
		 * 
		 * this function will use Memcached in case of sending $type parameter as 'memory'.
		 *
		 * @param      ConfigCollection  $config  config to be used for initializing the cache object 
		 * @param      string            $type    type of cache handler (currently only 'memory' is supported )
		 * @access public
		 */
		public function __construct(ConfigCollection $config, string $type)
		{
			parent::__construct();
			if ($type=='memory')
			{
				$this->handler = MemoryCache::getInstance($config);
			}
			else
			{
				throw new CacheException("'$type' is not a valid cache type.");
			}
		}

		/**
		 * Stores data into the cache for the $key.
		 * 
		 * use the handler store() function to store the supplied data based on the supplied key for the specified time.
		 * 
		 * @param string $cacheKey string key that referes to the current data
		 * @param mixed  $data data object to be cached.
		 * @param int    $expiryTime optional default 0 (cache never expire)
		 * @param string|null $folder folder to be used by memcached to store the $data
		 * @return bool whether the storing was succeed or not.
		 * @access public
		 */
		public function store(string $cacheKey, mixed $data, int $expiryTime = 0, ?string $folder=null):bool
		{
			return $this->handler->store($cacheKey, $data, $expiryTime, $folder);
		}
		
		/**
		 * Retrieves data from cache. 
		 * 
		 * User the cache handler retrieve function to get the cached data assosiated with a given key, returns NULL when no data is found.
		 * 
		 * @param string $cacheKey the key which was used to store the data.
		 * @param string|null $subFolder directory to search within for cached data.
		 * @return mixed
		 * @access public
		 */
		public function retrieve(string $cacheKey, ?string $subFolder=null):mixed
		{
			return $this->handler->retrieve($cacheKey, $subFolder);
		}
				
		/**
		 * Clears the cache. 
		 * 
		 * Use the cache handler clear function to clear all cached files within a directory. or all cached data in case of sending subFolder parameter as null.
		 * 
		 * @param string|null $subFolder directory to clear cached data within it.
		 * @return      bool true in case the clearing succeed.
		 * @access public
		 */
		public function clear(?string $subFolder=null):bool
		{
			return $this->handler->clear($subFolder);
		}
		
		/**
		* Removes the key from the cache.
		* 
		* this function will use the free() function from the cache handler to remove data from the cache assosiated with a given key and directory.
		* 
		* @param string $cacheKey the key used to save the data when {@link store()} function was called .
		* @param string $subFolder the directory used to save the data when {@link store()} function was called or null in case you called {@link store()} function and you supplied $folder parameter as null.
		* @return      bool true in case the clearing succeed.
		* @access public
		*/
		public function free(string $cacheKey, ?string $subFolder=null):bool
		{
			return $this->handler->free($cacheKey, $subFolder);
		}
	}
}
